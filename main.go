/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"fmt"
	"math/rand"
	"os"
	"test.io/recovery/api"
	"time"

	"github.com/spf13/cobra"
	"test.io/recovery/build"
	"test.io/recovery/recovery"
)

var (
	version = "dev"
	commit  = "none"
	date    = ""
)

func NewCommand() *cobra.Command {
	base := &cobra.Command{
		Use:  "influxd",
		Args: cobra.NoArgs, // 如果包含参数，则返回错误. (不需要传参的命令行可设置)
	}
	return base
}

func main() {
	if len(date) == 0 {
		date = time.Now().UTC().Format(time.RFC3339)
	}
	rand.Seed(time.Now().UnixNano())
	build.SetBuildInfo(version, commit, date)

	rootCmd := NewCommand()
	rootCmd.AddCommand(versionCmd())
	rootCmd.AddCommand(recovery.NewCommand())

	// add k8s api server command.
	rootCmd.AddCommand(api.NewAPIServerCommand())
	rootCmd.SilenceUsage = true
	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}

func versionCmd() *cobra.Command {
	return &cobra.Command{
		Use:   "version",
		Short: "Print the influxd server version",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Printf("Zac %s (git: %s) build_date: %s\n", version, commit, date)
		},
	}
}
