/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package api

import (
	cliflag "k8s.io/component-base/cli/flag"
	apiserverconfig "test.io/recovery/api/config"
	genericoptions "test.io/recovery/api/server"
)

type ServerRunOptions struct {
	ConfigFile             string
	DebugMode              bool
	GeneraServerRunOptions *genericoptions.ServerRunOptions
	*apiserverconfig.Config
}

func NewServerRunOptions() *ServerRunOptions {
	s := &ServerRunOptions{
		GeneraServerRunOptions: genericoptions.NewServerRunOptions(),
		Config:                 apiserverconfig.New(),
	}
	return s
}

func (s *ServerRunOptions) Validate() []error {
	var errors []error
	errors = append(errors, s.GeneraServerRunOptions.Validate()...)
	return errors
}

func (s *ServerRunOptions) Flags() (fss cliflag.NamedFlagSets) {
	fs := fss.FlagSet("generic")
	fs.BoolVar(&s.DebugMode, "debug", false, "Don't enable this if you don't know what it means.")

	s.GeneraServerRunOptions.AddFlags(fs, s.GeneraServerRunOptions)
	s.DevopsOptions.AddFlags(fss.FlagSet("devops"), s.DevopsOptions)

	return fss
}

func (s *ServerRunOptions) NewAPIServer(stopCh <-chan struct{}) error {

	return nil
}
