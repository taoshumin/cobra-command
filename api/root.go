/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package api

import (
	"context"
	"fmt"
	cliflag "k8s.io/component-base/cli/flag"
	"k8s.io/component-base/term"

	"github.com/spf13/cobra"
	utilerrors "k8s.io/apimachinery/pkg/util/errors"
	"sigs.k8s.io/controller-runtime/pkg/manager/signals"
)

func NewAPIServerCommand() *cobra.Command {
	s := NewServerRunOptions()
	// use viper read local file
	//conf, err := apiserverconfig.TryLoadFromDisk()
	//if err == nil {
	//	s = &ServerRunOptions{
	//		GeneraServerRunOptions: s.GeneraServerRunOptions,
	//		Config:                 conf,
	//	}
	//} else {
	//	klog.Fatal("Failed to load configuration from disk", err)
	//}

	base := &cobra.Command{
		Use: "k8s-apiserver", // the difference between Long and short.
		Long: `The KubeSphere API server validates and configures data for the API objects. 
The API Server services REST operations and provides the frontend to the
cluster's shared state through which all other components interact.`,
		RunE: func(cmd *cobra.Command, args []string) error {
			if errs := s.Validate(); len(errs) != 0 {
				return utilerrors.NewAggregate(errs)
			}
			return Run(s, signals.SetupSignalHandler())
		},
		SilenceUsage: false, // 发生错误的时候，禁默
	}

	fs := base.Flags()
	namedFlagSets := s.Flags()
	for _, f := range namedFlagSets.FlagSets {
		fs.AddFlagSet(f)
	}

	usageFmt := "Usage:\n %s\n"
	cols, _, _ := term.TerminalSize(base.OutOrStdout())
	base.SetHelpFunc(func(command *cobra.Command, strings []string) {
		_, _ = fmt.Fprintf(base.OutOrStdout(), "%s\n\n"+usageFmt, base.Long, base.UseLine())
		cliflag.PrintSections(base.OutOrStdout(), namedFlagSets, cols)
	})

	// TODO many sub commands.
	base.AddCommand(NewVersionCommand())
	return base
}

func Run(s *ServerRunOptions, ctx context.Context) error {
	if err := s.NewAPIServer(ctx.Done()); err != nil {
		return err
	}
	return nil
}
