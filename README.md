# recovery cobra command 生产使用方法

- 增加版本记录
- 增加cobra使用示例

# 示例

- 获取版本 （build/version build/build）
- k8s cobra示例： (api/root)
- 常规 cobra示例： (recovery/recovery)

# 安装

```go
go get -u github.com/spf13/cobra
```

# 创建

```go
cobra init --pkg-name /x6t.io/cobra -a 'taoshumin@sensetime.com'
或者
cd $GOPATH/src/x6t.io/cobra
cobra init --pkg-name . -a 'taoshumin@sensetime.com'
```

# 添加子命令

```go
cd GOPATH//src/x6t.io/cobra
cobra add version -a 'taoshumin@sensetime.com'
```

# Flags

### PersistentFlags (全局命令，所有命令及子命令均可使用)

```go
RootCmd.PersistentFlags().BoolVarP(&Verbose, "verbose", "v", false, "verbose output")
```

### Flags (局部命令，只能在指定的command中使用)

```go
RootCmd.Flags().StringVarP(&Source, "source", "s", "", "Source directory to read from")
```

### BindPFlag (绑定参数使用viper)

```go
RootCmd.PersistentFlags().StringVar(&author, "author", "YOUR NAME", "Author name for copyright attribution")
viper.BindPFlag("author", RootCmd.PersistentFlags().Lookup("author"))
```

# Viper使用文档

- [Viper使用文献](https://blog.51cto.com/u_15301988/3127353#_35)

# 参考文献

- [https://o-my-chenjian.com/2017/09/20/Using-Cobra-With-Golang/](https://o-my-chenjian.com/2017/09/20/Using-Cobra-With-Golang/)
- [https://blog.51cto.com/u_15301988/3127620](https://blog.51cto.com/u_15301988/3127620)