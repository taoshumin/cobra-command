/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package auth

import (
	"github.com/spf13/cobra"
	"os"
	"path/filepath"
)

func NewAuthCommand() *cobra.Command {
	base := &cobra.Command{
		Use:   "auth",
		Short: "On-disk authorization management commands, for recovery",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			cmd.PrintErrf("See '%s -h' for help\n", cmd.CommandPath())
		},
	}

	base.AddCommand(NewAuthListCommand())
	base.AddCommand(NewAuthCreateCommand())
	return base
}

type authListCommand struct {
	boltPath string
}

func (cmd *authListCommand) run() error {

	return nil
}

func NewAuthListCommand() *cobra.Command {
	var authCmd authListCommand
	cmd := &cobra.Command{
		Use:   "list",
		Short: "List authorizations",
		RunE: func(cmd *cobra.Command, args []string) error {
			// TODO Handler service.
			return authCmd.run()
		},
	}

	defaultPath := filepath.Join(os.Getenv("HOME"), ".influxdbv2", "influxd.bolt")
	cmd.Flags().StringVar(&authCmd.boltPath, "bolt-path", defaultPath, "Path to the BoltDB file.")
	return cmd
}

type authCreateCommand struct {
	boltPath string
	username string
	org      string
}

func (cmd *authCreateCommand) run() error {
	return nil
}

func NewAuthCreateCommand() *cobra.Command {
	var authCmd authCreateCommand
	cmd := &cobra.Command{
		Use:   "create-operator",
		Short: "Create new operator token for a user",
		RunE: func(cmd *cobra.Command, args []string) error {
			// 			authCmd.out = cmd.OutOrStdout()

			return authCmd.run()
		},
	}

	defaultPath := filepath.Join(os.Getenv("HOME"), ".influxdbv2", "influxd.bolt")
	cmd.Flags().StringVar(&authCmd.boltPath, "bolt-path", defaultPath, "Path to the BoltDB file")
	cmd.Flags().StringVar(&authCmd.username, "username", "", "Name of the user")
	cmd.Flags().StringVar(&authCmd.org, "org", "", "Name of the org")

	return cmd
}
